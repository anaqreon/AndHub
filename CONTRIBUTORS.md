<!--
This file contains references to people who contributed to the app.
You can also send a mail to [WIP](http://WIP) to get included.

Schema:  **[Name](Reference)**<br/>~° Text

Where:
  * Name: username, first/lastname
  * Reference: E-Mail, Webpage
  * Text: Information about / kind of contribution



## LIST OF CONTRIBUTORS
-->
**[Massimiliano](https://hub.disroot.org/channel/massimiliano)**<br/>~° Current developer of AndHub
