# AndHub
<img src="/app/src/main/ic_launcher-web.png" align="left" width="100" hspace="10" vspace="10">
This is an unofficial webview based client for the community-run, distributed social network <b><a href="https://project.hubzilla.org/page/hubzilla/hubzilla-project">Hubzilla</a></b>.

</br>
</br>

## Description
This is an unofficial webview based client for the community-run, distributed social network <b><a href="https://project.hubzilla.org/page/hubzilla/hubzilla-project/">Hubzilla</a></b>.
It's currently under development and should be used with that in mind. Please submit any bugs you might find.
We are basing our work on the dandelion* app

#### WebApp
The app is developed as a WebApp

Why is a WebApp better than using the mobile site on a browser?
Basically it provides better integration with the system (events coming into and going out of the app), notifications, customized interface and functions and a nice little icon that takes you directly to your favorite social network :)

#### Device Requirements
The minimum Android version supported is Jelly Bean, Android v4.2.0 / API 17

### Privacy & Permissions<a name="privacy"></a>
Hubzilla requires access to the Internet and to external storage to be able to upload photos when creating a new post and for taking screenshots.


## Contributions
The project is always open for contributions and accepts pull requests.
The project uses [AOSP Java Code Style](https://source.android.com/source/code-style#follow-field-naming-conventions), with one exception: private members are `_camelCase` instead of `mBigCamel`. You may use Android Studios _auto reformat feature_ before sending a PR. See [gsantner's android contribution guide](https://gsantner.net/android-contribution-guide/?packageid=com.github.dfa.diaspora_android&name=dandelion&web=https://github.com/gsantner/dandelion&source=readme#logcat) for more information.

Translations can be contributed on Framagit. You can use Stringlate ([![Translate - with Stringlate](https://img.shields.io/badge/stringlate-translate-green.svg)](https://lonamiwebs.github.io/stringlate/translate?git=https%3A%2F%2Fgithub.com%2Fgsantner%2Fdandelion.git)) to translate the project directly on your Android phone. It allows you to export as E-Mail attachement and to post on Framagit.

Note that the main project members are working on this project for free during leisure time, are mostly busy with their job/university/school, and may not react or start coding immediately.

#### Resources
* Project: [Changelog](/CHANGELOG.md) | [Issues level/beginner](https://framagit.org/disroot/AndHub/issues) | [License](/LICENSE.md) | [CoC](/CODE_OF_CONDUCT.md)
* Project AndHub account: [not there yet](https://)
* Hubzilla: [Framagit](https://framagit.org/hubzilla/core) | [Web](https://project.hubzilla.org) | [Hub HQ account](https://project.hubzilla.org/channel/hubzilla)
* F-droid: Not there yet.

## Licensing
ANndHub is released under GNU GENERAL PUBLIC LICENSE (see [LICENCE](https://framagit.org/disroot/AndHub/blob/master/LICENSE.md)).
The app is licensed GPL v3. Localization files and resources (strings\*.xml) are licensed CC0 1.0.
For more licensing informations, see [`3rd party licenses`](/app/src/main/res/raw/licenses_3rd_party.md).

## Screenshots
WIP

### Notice

#### Maintainers
